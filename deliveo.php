<?php

/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mav-IT Kft. <mail@mav-it.hu>
 * @copyright 2012-2024 Mavit Kft
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Mav-IT Kft.
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Deliveo extends Module
{
    const HOOKS = [

        'actionOrderGridDefinitionModifier',
        'actionOrderGridQueryBuilderModifier',
        'actionGetAdminOrderButtons',
        'displayBackOfficeHeader',
    ];

    protected $config_form = false;

    private $configFormKeys = array(
        'DELIVEO_API_KEY',
        'DELIVEO_LICENSE_KEY',
        'DELIVEO_NAME',
        'DELIVEO_COUNTRY_CODE',
        'DELIVEO_ZIP',
        'DELIVEO_CITY',
        'DELIVEO_ADDRESS',
        'DELIVEO_ADDRESS_2',
        'DELIVEO_PHONE',
        'DELIVEO_EMAIL',
        'DELIVEO_PACKAGE_X',
        'DELIVEO_PACKAGE_Y',
        'DELIVEO_PACKAGE_Z',
        'DELIVEO_PACKAGE_WEIGHT',
        'DELIVEO_PRIORITY',
        'DELIVEO_SATURDAY_DELIVERY',
        'DELIVEO_INSURANCE',
        'DELIVEO_ORDER_REF_NUM',
        'DELIVEO_SHIPPING_COST',
        'DELIVEO_SHIPPING_OPTION',
        'DELIVEO_CHANGE_ORDER_STATE',
        'DELIVEO_PACKAGING_UNIT',
    );

    public function __construct()
    {
        $this->name = 'deliveo';
        $this->tab = 'shipping_logistics';
        $this->version = '1.2.4';
        $this->author = 'Mav-IT Kft.';
        $this->need_instance = 1;

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Deliveo');
        $this->description = $this->l('Deliveo plugin for PrestaShop');

        $this->confirmUninstall = $this->l('Valóban törölni szeretné a modult?');

        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);


        $deliveoDir = _PS_MODULE_DIR_ . DIRECTORY_SEPARATOR . $this->name . DIRECTORY_SEPARATOR;

        require_once $deliveoDir . 'tools' . DIRECTORY_SEPARATOR . 'DeliveoCurlHelper.php';
        require_once $deliveoDir . 'tools' . DIRECTORY_SEPARATOR . 'DeliveoApiCalls.php';
    }

    /**
     * Install module
     *
     * @return bool
     */
    public function install()
    {
        //create database
        include dirname(__FILE__) . '/sql/install.php';

        //create hidden tab into menu
        $this->__installTab("DeliveoApiTab", $this->l("Deliveo"), 'AdminParentShipping');

        Configuration::updateValue('DELIVEO_SHIPPING_COST', 'felado');
        Configuration::updateValue('DELIVEO_PACKAGING_UNIT', '0');

        //install modul & create hook
        return parent::install() && $this->registerHook(self::HOOKS);
    }

    /**
     * Create tab into menu
     *
     * @param string $className
     * @param string $tabName
     * @param bool $tabParentName
     * @param bool|int $position
     *
     * @return int
     */
    private function __installTab($className, $tabName, $tabParentName = false, $position = false)
    {
        $tab = new Tab();

        if ($this->checkTabExists($className)) {
            $tab = new Tab(Tab::getIdFromClassName($className));
        }

        $tab->active = 0;
        $tab->class_name = $className;

        foreach (Language::getLanguages() as $lang) {
            $tab->name[$lang['id_lang']] = $tabName;
        }

        if ($position) {
            $tab->position = $position;
        }

        if ($tabParentName) {
            $tab->id_parent = Tab::getIdFromClassName($tabParentName);
        } else {
            $tab->id_parent = 0;
        }

        $tab->module = $this->name;

        $tab->add();
    }

    /**
     * Check tab exists
     *
     * @param string $className
     *
     * @return false|null|string
     */
    private function checkTabExists($className)
    {
        $sql = "SELECT COUNT(*)
                FROM `" . _DB_PREFIX_ . "tab`
                WHERE `module` = '{$this->name}'
                AND `class_name` = '{$className}'";

        return Db::getInstance()->getValue($sql);
    }

    /**
     * Uninstall module
     *
     * @return bool
     */
    public function uninstall()
    {
        //Delete saved configuration values
        foreach ($this->configFormKeys as $key) {
            Configuration::deleteByName($key);
        }

        //drop database tables
        include dirname(__FILE__) . '/sql/uninstall.php';

        //remove tab from menu
        $this->uninstallTab('DeliveoApiTab');

        //uninstall module
        return parent::uninstall();
    }

    /**
     * Remove tab from menu
     *
     * @param string $className
     */
    private function uninstallTab($className)
    {
        if ($this->checkTabExists($className)) {
            $tab = new Tab(Tab::getIdFromClassName($className));

            if ($tab) {
                $tab->delete();
            }
        }
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool) Tools::isSubmit('submitDeliveoModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $this->context->smarty->assign('form_data', $this->getConfigFormValues());
        $api = DeliveoApiCalls::getInstance();

        try {
            $shippingOptions = $api->getShippingOptions('HU');

            if ($shippingOptions["type"] == "success") {
                $shippingOptions = $shippingOptions["data"];
            }
        } catch (Exception $e) {
            $shippingOptions = array();
        }

        $this->context->smarty->assign('shippingOptions', $shippingOptions);
        $this->context->smarty->assign('orderStates', OrderState::getOrderStates($this->context->language->id));

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $output;
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {

        $values = array();
        foreach ($this->configFormKeys as $key) {
            $values[$key] = Configuration::get($key);
        }

        return $values;
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    public function hookActionGetAdminOrderButtons(array $params) {}

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookDisplayBackOfficeHeader()
    {
        //add api url to head
        $link = new Link();
        $deliveoLink = str_replace('http:', '', $link->getAdminLink('DeliveoApiTab', true, array(), ["action" => "send"]));
        $deliveoPackageLog = $link->getAdminLink('DeliveoApiTab', true, array(), ["action" => "getPackageLog"]);
        $deliveoUrl = "//api.deliveo.eu/";
        $deliveoSlug = "?api_key=" . Configuration::get('DELIVEO_API_KEY') . "&licence=" . Configuration::get('DELIVEO_LICENSE_KEY');
        Media::addJsDef(['deliveoApiLink' => $deliveoLink]);
        Media::addJsDef(['deliveoPackageLog' => $deliveoPackageLog]);
        Media::addJsDef(['deliveoUrl' => $deliveoUrl]);
        Media::addJsDef(['deliveoSlug' => $deliveoSlug]);

        //add css & js files to head
        $this->context->controller->addJS($this->_path . 'views/js/back.js');
        $this->context->controller->addCSS($this->_path . 'views/css/back.css');
    }

    /**
     * Hook allows to modify Order grid definition since 1.7.7.0
     *
     * @param array $params
     */
    public function hookActionOrderGridDefinitionModifier(array $params)
    {
        if (empty($params['definition'])) {
            return;
        }

        /** @var PrestaShop\PrestaShop\Core\Grid\Definition\GridDefinitionInterface $definition */
        $definition = $params['definition'];

        $column = new PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\BadgeColumn('Deliveo');
        $column->setName('Deliveo');
        $column->setOptions([
            'field' => 'Deliveo',
            'badge_type' => 'success',
            'empty_value' => '',
            'clickable' => false,
        ]);

        $definition
            ->getColumns()
            ->addAfter(
                'osname',
                $column
            );

        $definition
            ->getBulkActions()
            ->add((new \PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type\ButtonBulkAction('new_action'))
                    ->setName($this->trans('Feladás Deliveo-ba'))
                    ->setOptions([
                        'class' => 'js-send-deliveo',
                    ])
            );
    }
    /**
     * Hook allows to modify Order grid data since 1.7.7.0
     *
     * @param array $params
     */
    public function hookActionOrderGridDataModifier(array $params)
    {
        if (empty($params['data'])) {
            return;
        }

        /** @var PrestaShop\PrestaShop\Core\Grid\Data\GridData $gridData */
        $gridData = $params['data'];
        $modifiedRecords = $gridData->getRecords()->all();


        foreach ($modifiedRecords as $key => $data) {
            $modifiedRecords[$key]['Deliveo'] = $data['Deliveo'];
        }

        $params['data'] = new PrestaShop\PrestaShop\Core\Grid\Data\GridData(
            new PrestaShop\PrestaShop\Core\Grid\Record\RecordCollection($modifiedRecords),
            $gridData->getRecordsTotal(),
            $gridData->getQuery()
        );
    }

    /**
     * Hook allows to modify Order query builder and add custom sql statements since 1.7.7.0
     *
     * @param array $params
     */
    public function hookActionOrderGridQueryBuilderModifier(array $params)
    {
        if (empty($params['search_query_builder']) || empty($params['search_criteria'])) {
            return;
        }

        /** @var Doctrine\DBAL\Query\QueryBuilder $searchQueryBuilder */
        $searchQueryBuilder = $params['search_query_builder'];

        /** @var PrestaShop\PrestaShop\Core\Search\Filters\OrderFilters $searchCriteria */
        $searchCriteria = $params['search_criteria'];

        $searchQueryBuilder->addSelect(
            'dp.`dp_response_code` AS `Deliveo`'
        );

        $searchQueryBuilder->leftJoin(
            'o',
            '`' . _DB_PREFIX_ . 'deliveo_packages`',
            'dp',
            'dp.`dp_package_ref` = o.`id_order`'
        );

        foreach ($searchCriteria->getFilters() as $filterName => $filterValue) {
            if ('deliveo' === $filterName) {
                $searchQueryBuilder->andWhere('dp.`id_reference` = :deliveo_reference');
                $searchQueryBuilder->setParameter('deliveo_reference', $filterValue);
            }
        }
    }
}
