<?php

/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mav-IT Kft. <mail@mav-it.hu>
 * @copyright 2012-2024 Mavit Kft
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Mav-IT Kft.
 */

class DeliveoApiCalls
{

    private static $instance = null;

    /**
     * @return DeliveoApiCalls|null
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Csomag feladása
     * POST
     *
     * @param array $orderData
     *
     * @return mixed
     */
    public function sendOrder($orderData)
    {
        $orderData['sender'] = Configuration::get('DELIVEO_NAME');
        $orderData['sender_country'] = Configuration::get('DELIVEO_COUNTRY_CODE');
        $orderData['sender_zip'] = Configuration::get('DELIVEO_ZIP');
        $orderData['sender_city'] = Configuration::get('DELIVEO_CITY');
        $orderData['sender_address'] = Configuration::get('DELIVEO_ADDRESS');
        $orderData['sender_apartment'] = Configuration::get('DELIVEO_ADDRESS_2');
        $orderData['sender_phone'] = Configuration::get('DELIVEO_PHONE');
        $orderData['sender_email'] = Configuration::get('DELIVEO_EMAIL');
        $orderData['priority'] = Configuration::get('DELIVEO_PRIORITY');
        $orderData['optional_parameter_1'] = Configuration::get('DELIVEO_PRIORITY');
        $orderData['optional_parameter_3'] = Configuration::get('DELIVEO_SATURDAY_DELIVERY');
        $orderData['optional_parameter_2'] = Configuration::get('DELIVEO_INSURANCE');

        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/package/create")
            ->setPostData($orderData)
            ->send();

        return $call->getCurlResult();
    }

    /**
     * Szállítási opciók lekérése
     * GET
     *
     * @param string $language
     *
     * @return mixed
     */
    public function getShippingOptions($language)
    {
        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/delivery", ["lang" => $language])
            ->get();
        return $call->getCurlResult();
    }

    /**
     * Csomagnapló lekérése
     * GET
     *
     * @param string $group_id
     *
     * @return mixed
     */
    public function getPackageLog($group_id)
    {
        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/package_log/" . $group_id, [])
            ->get();

        die(json_encode($call->getCurlResult()));
    }

    /**
     * Sikertelen állapotok lekérése
     * GET
     *
     * @return mixed
     */
    public function getUnsuccessful()
    {
        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/unsuccessful")
            ->get();

        return $call->getCurlResult();
    }

    /**
     * Kötelező mezők lekérése
     * GET
     *
     * @return mixed
     */
    public function getRequiredFields()
    {
        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/required")
            ->get();

        return $call->getCurlResult();
    }

    /**
     * Elérhető nyelvek
     * GET
     *
     * @return mixed
     */
    public function getSupportedLanguages()
    {
        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/lang")
            ->get();

        return $call->getCurlResult();
    }

    /**
     * Csomaghelyek lekérése
     * GET
     *
     * @return mixed
     */
    public function getPackageLocations()
    {
        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/locations")
            ->get();

        return $call->getCurlResult();
    }

    /**
     * Csomagok lekérése
     * GET
     *
     * @return mixed
     */
    public function getPackages()
    {
        $call = DeliveoCurlHelper::getInstance()
            ->setUrl("/package")
            ->get();

        return $call->getCurlResult();
    }
}
