<?php

/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mav-IT Kft. <mail@mav-it.hu>
 * @copyright 2012-2024 Mavit Kft
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Mav-IT Kft.
 */

/**
 * Class DeliveoCurlHelper
 */
class DeliveoCurlHelper
{
    /**
     * @var null
     */
    private static $instance = null;
    /**
     * @var
     */
    private $ch;
    /**
     * @var
     */
    private $url;
    /**
     * @var
     */
    private $postData;
    /**
     * @var bool
     */
    private $curlResult = false;

    /**
     * @return DeliveoCurlHelper|null
     */
    public static function getInstance()
    {
        if (!is_object(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param string $url
     * @param array $additionalParameters
     *
     * @return $this
     */
    public function setUrl($url, $additionalParameters = array())
    {
        $licenseKey = Configuration::get('DELIVEO_LICENSE_KEY');
        $apiKey = Configuration::get('DELIVEO_API_KEY');

        $parameters = [
            "licence" => $licenseKey,
            "api_key" => $apiKey,
        ];
        $parameters = array_merge($parameters, $additionalParameters);
        $query = http_build_query($parameters);

        $this->url = "https://api.deliveo.eu{$url}?{$query}";

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getPostData($asJson = false)
    {
        if (!$asJson) {
            return $this->postData;
        }

        return json_encode($this->postData);
    }

    /**
     * @param mixed $postData
     */
    public function setPostData(array $postData)
    {
        $this->postData = $postData;

        return $this;
    }

    /**
     * cURL POST
     *
     * @return $this
     */
    public function send()
    {
        $postData = $this->getPostData(false);

        $this->ch = curl_init();
        curl_setopt_array(
            $this->ch,
            array(
                CURLOPT_URL => $this->getUrl(),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($postData),
                CURLOPT_FAILONERROR => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT => 'Deliveo Prestashop Module',
                CURLOPT_HTTPHEADER => array('Source: Prestashop')
            )
        );
        $this->setCurlResult(curl_exec($this->ch));
        curl_close($this->ch);

        return $this;
    }

    /**
     * cURL GET
     *
     * @return $this
     */
    public function get()
    {
        $this->ch = curl_init();
        curl_setopt_array($this->ch, array(
            CURLOPT_URL => $this->getUrl(),
            CURLOPT_FAILONERROR => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'Deliveo Prestashop Module',
            CURLOPT_HTTPHEADER => array('Source: Prestashop')

        ));
        $this->setCurlResult(curl_exec($this->ch));
        curl_close($this->ch);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurlResult()
    {
        return json_decode($this->curlResult, true);
    }

    /**
     * @param mixed $curlResult
     */
    public function setCurlResult($curlResult)
    {
        $this->curlResult = $curlResult;
    }
}
