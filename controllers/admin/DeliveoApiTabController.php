<?php

/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mav-IT Kft. <mail@mav-it.hu>
 * @copyright 2012-2024 Mavit Kft
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Mav-IT Kft.
 */

class DeliveoApiTabController extends ModuleAdminController
{

    public function __construct()
    {
        parent::__construct();

        if (class_exists('Deliveo')) {
            $this->deliveoModule = new Deliveo();
        }
        $deliveoDir = _PS_MODULE_DIR_ . DIRECTORY_SEPARATOR . 'deliveo' . DIRECTORY_SEPARATOR;
        require_once $deliveoDir . 'tools' . DIRECTORY_SEPARATOR . 'DeliveoCurlHelper.php';
        require_once $deliveoDir . 'tools' . DIRECTORY_SEPARATOR . 'DeliveoApiCalls.php';
        require_once $deliveoDir . 'models' . DIRECTORY_SEPARATOR . 'DeliveoPackages.php';
    }

    public function initContent()
    {
        parent::initContent();

        $action = Tools::getValue('action', false);


        if ($action) {
            switch ($action) {
                case 'send':
                    $this->sendDataToDeliveo(Tools::getValue('selectedOrders'));
                    break;
                case 'getPackageLog':
                    $this->getPackageLog(Tools::getValue('group_id'));
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Sending orders into Deliveo
     *
     * @param int $group_id
     *
     * @throws PrestaShopException
     */
    private function getPackageLog($group_id)
    {
        $api = DeliveoApiCalls::getInstance();
        return $api->getPackageLog($group_id);
    }

    /**
     * Sending orders into Deliveo
     *
     * @param array $orderIds
     *
     * @throws PrestaShopException
     */
    private function sendDataToDeliveo(array $orderIds)
    {
        $errors = [];
        $success = [];

        $added = 0;

        if ($orderIds) {
            foreach ($orderIds as $o) {
                $model = new DeliveoPackages();
                $order = new Order($o['id_order']);

                //check order has Deliveo code
                if ($order && $model->checkExists($order->id) === false) {
                    //get customer address
                    $address = $this->getCustomerAddressInformation($order);
                    //get customer data
                    $customer = $this->getCustomer($order->id_customer);

                    //api class instance
                    $api = DeliveoApiCalls::getInstance();

                    //set the package details
                    $orderData = [
                        "consignee" => "{$address->lastname} {$address->firstname}",
                        "consignee_country" => $address->country,
                        "consignee_zip" => $address->postcode,
                        "consignee_city" => $address->city,
                        "consignee_address" => $address->address1,
                        "consignee_apartment" => $address->address2,
                        "consignee_phone" => $address->phone,
                        "consignee_email" => $customer->email,
                        "delivery" => Configuration::get('DELIVEO_SHIPPING_OPTION'),
                        "cod" => $order->total_paid_real,
                        "freight" => Configuration::get('DELIVEO_SHIPPING_COST'),
                        "packages" => $this->addPackages($order),
                    ];


                    switch ((int)Configuration::get('DELIVEO_PACKAGING_UNIT')) {
                        case 0:
                            $orderData['colli'] = 1;
                            break;
                        case 1:
                            $orderData['colli'] = $o['packaging_unit'];
                            break;
                        case 2:
                            $orderData['colli'] = $o['packaging_unit'];
                            break;
                    }


                    if ($result = $api->sendOrder($orderData)) {
                        //when send was failed
                        if ($result["type"] == "error" || $result["type"] == "warning") {
                            $errorMessage = isset($result['msg']) ? $result['msg'] : 'Error code ' . $result['error_code'];
                            if (isset($result["field"])) {
                                $errorMessage .= ' (' . $this->__getResponseFieldName($result["field"]) . ')';
                            }
                            $errors[] = $order->reference . ': ' . $errorMessage;
                        } elseif ($result["type"] == "success") {
                            //save deliveo data
                            $model->dp_package_ref = $order->id;
                            $model->dp_response_code = $result["data"][0];
                            $model->dp_response = $result["msg"];
                            $model->save();

                            $this->changeOrderState($o['id_order']);
                            $success[] = $order->reference . ':  ' . $result["data"][0];
                            $added++;
                        }
                    }
                } else {
                    $errors[] = $order->reference . ': Package sent already!';
                }
            }
            die(json_encode(
                [
                    'type' => 'success',
                    'success' => $success,
                    'errors'   => $errors
                ]
            ));
        }
    }

    /**
     * Get Customer address for order
     *
     * @param Order $order
     *
     * @return Address
     */
    private function getCustomerAddressInformation(Order $order)
    {
        // Retrieve addresses information
        $addressInvoice = new Address($order->id_address_invoice, $this->context->language->id);

        if ($order->id_address_invoice == $order->id_address_delivery) {
            $addressDelivery = $addressInvoice;
        } else {
            $addressDelivery = new Address($order->id_address_delivery, $this->context->language->id);
        }
        return $addressDelivery;
    }

    /**
     * Get Customer details
     *
     * @param int $customerID
     *
     * @return Customer
     */
    private function getCustomer($customerID)
    {
        $customer = new Customer($customerID);
        return $customer;
    }

    private function addPackages(Order $order)
    {
        $packages = array();

        $products = $order->getProducts();

        if ($products) {
            foreach ($products as $row) {
                $weight = Configuration::get('DELIVEO_PACKAGE_WEIGHT');
                $x = Configuration::get('DELIVEO_PACKAGE_X');
                $y = Configuration::get('DELIVEO_PACKAGE_Y');
                $z = Configuration::get('DELIVEO_PACKAGE_Z');
                $productID = $row['product_id'] . ' ' . $row['product_attribute_id'];

                if ($row['product_weight'] > 0) {
                    $weight = $row['product_weight'];
                }

                if ($row['weight'] > 0) {
                    $weight = $row['weight'];
                }

                if ($row['width'] > 0) {
                    $x = $row['width'];
                }

                if ($row['height'] > 0) {
                    $y = $row['height'];
                }

                if ($row['depth'] > 0) {
                    $z = $row['depth'];
                }

                if ($row['reference']) {
                    $productID = $row['reference'];
                }

                if ($row['ean13']) {
                    $productID = $row['ean13'];
                }

                if ($row['isbn']) {
                    $productID = $row['isbn'];
                }

                if ($row['upc']) {
                    $productID = $row['upc'];
                }

                for ($i = 0; $i < (int) $row['product_quantity']; $i++) {
                    $packages[] = array(
                        'weight' => $weight, // a csomag súlya
                        'x' => $x, // csomag szélessége
                        'y' => $y, // csomag magassága
                        'z' => $z, // csomag mélysége
                        'item_no' => $productID, //cikkszám
                    );
                }
            }
        }
        return $packages;
    }

    /**
     * Change order state
     *
     * @param int $id_order
     */
    private function changeOrderState($id_order)
    {
        $id_order_state = Configuration::get('DELIVEO_CHANGE_ORDER_STATE');
        $order_state = new OrderState($id_order_state);

        if (Validate::isLoadedObject($order_state)) {
            $order = new Order((int) $id_order);
            if (Validate::isLoadedObject($order)) {
                $current_order_state = $order->getCurrentOrderState();
                if ($current_order_state->id !== $order_state->id) {
                    $history = new OrderHistory();
                    $history->id_order = $order->id;
                    $history->id_employee = (int) $this->context->employee->id;

                    $use_existings_payment = !$order->hasInvoice();
                    $history->changeIdOrderState((int) $order_state->id, $order, $use_existings_payment);

                    $carrier = new Carrier($order->id_carrier, $order->id_lang);
                    $templateVars = array();
                    if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number) {
                        $templateVars = array(
                            '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
                        );
                    }

                    if ($history->addWithemail(true, $templateVars)) {
                        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT')) {
                            foreach ($order->getProducts() as $product) {
                                if (StockAvailable::dependsOnStock($product['product_id'])) {
                                    StockAvailable::synchronize($product['product_id'], (int) $product['id_shop']);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private function __getResponseFieldName($key)
    {
        $fieldNames = $this->__responseFieldNames();
        return isset($fieldNames[$key]) ? $fieldNames[$key] : $key;
    }

    private function __responseFieldNames()
    {
        return array(
            "group_id" => $this->deliveoModule->l("Csoportkód"),
            "sender" => $this->deliveoModule->l("Feladó neve"),
            "sender_country" => $this->deliveoModule->l("Feladó ország"),
            "sender_zip" => $this->deliveoModule->l("Feladó irányítószáma"),
            "sender_city" => $this->deliveoModule->l("Feladó település"),
            "sender_address" => $this->deliveoModule->l("Feladó utca, házszám"),
            "sender_apartment" => $this->deliveoModule->l("Feladó épület, emelet, ajtó"),
            "sender_phone" => $this->deliveoModule->l("Feladó telefonszáma"),
            "sender_email" => $this->deliveoModule->l("Feladó e-mail címe"),
            "consignee" => $this->deliveoModule->l("Címzett neve"),
            "consignee_country" => $this->deliveoModule->l("Címzett ország"),
            "consignee_zip" => $this->deliveoModule->l("Címzett irányítószáma"),
            "consignee_city" => $this->deliveoModule->l("Címzett település"),
            "consignee_address" => $this->deliveoModule->l("Címzett utca, házszám"),
            "consignee_apartment" => $this->deliveoModule->l("Címzett épület, emelet, ajtó"),
            "consignee_phone" => $this->deliveoModule->l("Címzett telefonszáma"),
            "consignee_email" => $this->deliveoModule->l("Címzett e-mail címe"),
            "delivery" => $this->deliveoModule->l("Szállítási opció"),
            "delivery_id" => $this->deliveoModule->l("Szállítási opció azonosítója"),
            "picked_up_location_id" => $this->deliveoModule->l("Rendszám, aki behozta"),
            "picked_up" => $this->deliveoModule->l("Futár felvette"),
            "dropped_off" => $this->deliveoModule->l("Futár leadta"),
            "location_id" => $this->deliveoModule->l("Aktuális csomaghely/rendszám"),
            "received_by" => $this->deliveoModule->l("Átvette"),
            "unsuccessful" => $this->deliveoModule->l("Sikertelen állapot"),
            "unsuccessful_id" => $this->deliveoModule->l("Sikertelen állapot azonosítója"),
            "priority" => $this->deliveoModule->l("Opcionális paraméter #1"),
            "saturday" => $this->deliveoModule->l("Opcionális paraméter #3"),
            "insurance" => $this->deliveoModule->l("Opcionális paraméter #2"),
            "referenceid" => $this->deliveoModule->l("Hivatkozási szám okmánykezelés esetén"),
            "cod" => $this->deliveoModule->l("Utánvét összege"),
            "freight" => $this->deliveoModule->l("Fuvardíjat fizeti"),
            "comment" => $this->deliveoModule->l("Megjegyzés"),
            "currency" => $this->deliveoModule->l("Deviza"),
            "last_modified" => $this->deliveoModule->l("Utolsó módosítás UTC-ben"),
        );
    }
}
