{**
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author Mav-IT Kft. <mail@mav-it.hu>
    * @copyright 2012-2024 Mavit Kft
    * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
    * International Registered Trademark & Property of Mav-IT Kft.
    *}

<form id="module_form" class="defaultForm form-horizontal" method="post">
    <input type="hidden" name="submitDeliveoModule" value="1">
    <div class="panel">
        <h3><i class="icon icon-cogs"></i> {l s='Beállítások' mod='deliveo'}</h3>

        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='API kulcs' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_API_KEY" id="DELIVEO_API_KEY" value="{$form_data['DELIVEO_API_KEY']}" required="required">
                    <p class="help-block">
                        {l s='A szerződött futárszolgálat adja meg' mod='deliveo'}
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Licensz' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_LICENSE_KEY" id="DELIVEO_LICENSE_KEY" value="{$form_data['DELIVEO_LICENSE_KEY']}" required="required">
                    <p class="help-block">
                        {l s='A szerződött futárszolgálat adja meg' mod='deliveo'}
                    </p>
                </div>
            </div>
        </div>

    </div>
    <div class="panel">
        <h3>
            <i class="icon icon-cogs"></i> {l s='Feladó beállítása | Az itt megadott paraméterek kerülnek a csomag adataiba mint "Feladó"'
                mod='deliveo'}
        </h3>

        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Feladó neve' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_NAME" id="DELIVEO_NAME" value="{$form_data['DELIVEO_NAME']}" required="required">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Feladó országának kétjegyű kódja' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_COUNTRY_CODE" id="DELIVEO_COUNTRY_CODE" value="{$form_data['DELIVEO_COUNTRY_CODE']}" length="2" required="required">
                    <p class="help-block">
                        {l s='Pl. "HU", "DE"' mod='deliveo'}, a országkódok listáját <a href="https://hu.wikipedia.org/wiki/Orsz%C3%A1gok_%C3%A9s_ter%C3%BCletek_k%C3%B3djainak_list%C3%A1ja" target="_blank"><strong>itt</strong></a> találja
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Feladó településének irányítószáma' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_ZIP" id="DELIVEO_ZIP" value="{$form_data['DELIVEO_ZIP']}" required="required">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Feladó település neve' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_CITY" id="DELIVEO_CITY" value="{$form_data['DELIVEO_CITY']}" required="required">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Feladó közterület neve, házszám' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_ADDRESS" id="DELIVEO_ADDRESS" value="{$form_data['DELIVEO_ADDRESS']}" required="required">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Feladó épület, lépcsőház, emelet, ajtó' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_ADDRESS_2" id="DELIVEO_ADDRESS_2" value="{$form_data['DELIVEO_ADDRESS_2']}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Feladó telefonszám' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_PHONE" id="DELIVEO_PHONE" value="{$form_data['DELIVEO_PHONE']}" required="required">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Feladó e-mail' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_EMAIL" id="DELIVEO_EMAIL" value="{$form_data['DELIVEO_EMAIL']}" required="required">
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn" name="submitDeliveoModule" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Mentés' mod='deliveo'}
            </button>
        </div>

    </div>


    <div class="panel">
        <h3>
            <i class="icon icon-cogs"></i> {l s='Alapértelmezett csomag méretek | Ha hiányoznak a termék méretei, akkor ezeket használjuk
                feladáskor.' mod='deliveo'}
        </h3>

        <div class="form-wrapper">
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='X = szélesség' mod='deliveo'}
                </label>
                <div class="col-lg-2">
                    <div class="input-group">
                        <input type="number" name="DELIVEO_PACKAGE_X" id="DELIVEO_PACKAGE_X" min="1" value="{$form_data['DELIVEO_PACKAGE_X']}" class="form-control" required>
                        <span class="input-group-addon">cm</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Y = magasság' mod='deliveo'}
                </label>
                <div class="col-lg-2">
                    <div class="input-group">
                        <input type="number" name="DELIVEO_PACKAGE_Y" id="DELIVEO_PACKAGE_Y" min="1" value="{$form_data['DELIVEO_PACKAGE_Y']}" class="form-control" required>
                        <span class="input-group-addon">cm</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Z = mélység' mod='deliveo'}
                </label>
                <div class="col-lg-2">
                    <div class="input-group">
                        <input type="number" name="DELIVEO_PACKAGE_Z" id="DELIVEO_PACKAGE_Z" min="1" value="{$form_data['DELIVEO_PACKAGE_Z']}" class="form-control" required>
                        <span class="input-group-addon">cm</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 required">
                    {l s='Súly' mod='deliveo'}
                </label>
                <div class="col-lg-2">
                    <div class="input-group">
                        <input type="number" name="DELIVEO_PACKAGE_WEIGHT" id="DELIVEO_PACKAGE_WEIGHT" min="1" value="{$form_data['DELIVEO_PACKAGE_WEIGHT']}" class="form-control" required>
                        <span class="input-group-addon">kg</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" value="1" id="module_form_submit_btn" name="submitDeliveoModule" class="btn btn-default pull-right">
                <i class="process-icon-save"></i> {l s='Mentés' mod='deliveo'}
            </button>
        </div>

    </div>

    <div class="panel">
        <h3>
            {l s='Csomagolási egység' mod='deliveo'}
        </h3>
        <div class="form-group">
            <label class="control-label col-lg-3">
                {l s='Csomagok száma' mod='deliveo'}
            </label>
            <div class="col-lg-3">
                <span class=" fixed-width-lg">
                    <select name="DELIVEO_PACKAGING_UNIT">
                        <option {if $form_data['DELIVEO_PACKAGING_UNIT']==0 }selected {/if} value="0">
                            {l s='Mindig 1 csomagként kezelje' mod='deliveo'}
                        </option>
                        <option {if $form_data['DELIVEO_PACKAGING_UNIT']==1 }selected {/if} value="1">
                            {l s='Tételenként egy' mod='deliveo'}
                        </option>
                    </select>
                    <a class="slide-button btn"></a>
                </span>

            </div>
        </div>

    </div>
    <div class="panel">
        <h3>
            <i class="icon icon-cogs"></i> {l s='Szállítási paraméterek | Ezekkel a paraméterekkel lesz feladva minden csomag.' mod='deliveo'}
        </h3>

        <div class="form-wrapper">

            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Opcionális paraméter #1' mod='deliveo'}

                </label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="DELIVEO_PRIORITY" id="DELIVEO_PRIORITY_on" value="1" {if $form_data['DELIVEO_PRIORITY']==1 }checked{/if} /> <label for="DELIVEO_PRIORITY_on">
                            {l s='Igen' mod='deliveo'}
                        </label>
                        <input type="radio" name="DELIVEO_PRIORITY" id="DELIVEO_PRIORITY_off" value="0" {if $form_data['DELIVEO_PRIORITY']==0 }checked{/if} /> <label for="DELIVEO_PRIORITY_off">
                            {l s='Nem' mod='deliveo'}
                        </label>
                        <a class="slide-button btn"></a>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Opcionális paraméter #2' mod='deliveo'}

                </label>
                <div class="col-lg-3">
                    <input type="text" name="DELIVEO_INSURANCE" id="DELIVEO_INSURANCE" value="{$form_data['DELIVEO_INSURANCE']}">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Opcionális paraméter #3' mod='deliveo'}
                </label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="DELIVEO_SATURDAY_DELIVERY" id="DELIVEO_SATURDAY_DELIVERY_on" value="1" {if $form_data['DELIVEO_SATURDAY_DELIVERY']==1 }checked{/if} /> <label for="DELIVEO_SATURDAY_DELIVERY_on">
                            {l s='Igen' mod='deliveo'}
                        </label>
                        <input type="radio" name="DELIVEO_SATURDAY_DELIVERY" id="DELIVEO_SATURDAY_DELIVERY_off" value="0" {if $form_data['DELIVEO_SATURDAY_DELIVERY']==0 }checked{/if} /> <label for="DELIVEO_SATURDAY_DELIVERY_off">
                            {l s='Nem' mod='deliveo'}
                        </label>
                        <a class="slide-button btn"></a>
                    </span>

                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Rendelésszám kerüljön a Hivatkozási szám mezőbe' mod='deliveo'}
                </label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="DELIVEO_ORDER_REF_NUM" id="DELIVEO_ORDER_REF_NUM_on" value="1" {if $form_data['DELIVEO_ORDER_REF_NUM']==1 }checked{/if} /> <label for="DELIVEO_ORDER_REF_NUM_on">
                            {l s='Igen' mod='deliveo'}
                        </label>
                        <input type="radio" name="DELIVEO_ORDER_REF_NUM" id="DELIVEO_ORDER_REF_NUM_off" value="0" {if $form_data['DELIVEO_ORDER_REF_NUM']==0 }checked{/if} /> <label for="DELIVEO_INSURANCE_off">
                            {l s='Nem' mod='deliveo'}
                        </label>
                        <a class="slide-button btn"></a>
                    </span>
                    <p class="help-block">
                        {l s='Használat előtt egyeztessen a szerződött futárszolgálattal!' mod='deliveo'}
                    </p>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='Ki fizeti a szállítási költséget' mod='deliveo'}
                </label>
                <div class="col-lg-9">
                    <span class="switch prestashop-switch fixed-width-lg">
                        <input type="radio" name="DELIVEO_SHIPPING_COST" id="DELIVEO_SHIPPING_COST_owner" value="felado" {if $form_data['DELIVEO_SHIPPING_COST']=='felado' }checked{/if} /> <label for="DELIVEO_SHIPPING_COST_owner">
                            {l s='Feladó' mod='deliveo'}
                        </label>
                        <input type="radio" name="DELIVEO_SHIPPING_COST" id="DELIVEO_ORDER_REF_NUM_customer" value="cimzett" {if $form_data['DELIVEO_ORDER_REF_NUM']=='cimzett' }checked{/if} /> <label for="DELIVEO_ORDER_REF_NUM_customer">
                            {l s='Címzett' mod='deliveo'}
                        </label>
                        <a class="slide-button btn"></a>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">
                    {l s='A feladottak állapotát erre módosítsa' mod='deliveo'}
                </label>
                <div class="col-lg-3">
                    <select name="DELIVEO_CHANGE_ORDER_STATE" id="order_states">
                        {if $orderStates}
                            {foreach from=$orderStates item=order}
                                <option value="{$order['id_order_state']}" {if $form_data['DELIVEO_CHANGE_ORDER_STATE']==$order['id_order_state']} selected="selected" {/if}> {$order['name']} </option> {/foreach} {/if}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">
                            {l s='Szállítási opciók (a Deliveo-ból lekérdezve)' mod='deliveo'}
                        </label>
                        <div class="col-lg-3">
                            <select name="DELIVEO_SHIPPING_OPTION" id="shipping_options">
                                {if $shippingOptions}
                                    {foreach from=$shippingOptions item=ship}
                                        <option value="{$ship['value']}" {if $form_data['DELIVEO_SHIPPING_OPTION']==$ship['value']} selected="selected" {/if}> {$ship['description']} </option> {/foreach} {/if}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" value="1" id="module_form_submit_btn" name="submitDeliveoModule" class="btn btn-default pull-right">
                                <i class="process-icon-save"></i> {l s='Mentés' mod='deliveo'}
                            </button>
                        </div>

                    </div>
                </form>
                < class="bootstrap">
                    <div class="modal fade" id="modules_list_container">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 class="modal-title">Ajánlott modulok és szolgáltatások</h3>
                                </div>
                                <div class="modal-body">
                                    <div id="modules_list_container_tab_modal" style="display:none;"></div>
                                    <div id="modules_list_loader"><i class="icon-refresh icon-spin"></i></div>
                                </div>
                            </div>
                        </div>
                </div>