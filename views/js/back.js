/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mav-IT Kft. <mail@mav-it.hu>
 * @copyright 2012-2024 Mavit Kft
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of Mav-IT Kft.
 */

jQuery(document).ready(function () {
  jQuery('.js-send-deliveo').on('click', function () {
    sendDeliveo();
  });
});

function sendDeliveo() {
  var orderIds = [];

  var versionPre177 = true;

  var form = jQuery('#form-order');
  if (form.length == 0) {
    versionPre177 = false;
    form = jQuery('#order_filter_form');
  }

  if (typeof form !== 'undefined' && form) {
    var checkboxes =
      versionPre177 ? form.find('input[name="orderBox[]"]') : form.find('.js-bulk-action-checkbox');

    if (checkboxes && checkboxes.length > 0) {
      jQuery.each(checkboxes, function (key, row) {

        if (jQuery(row).is(':checked')) {
          orderIds.push(
            {
              "id_order": jQuery(row).val(),
              "packaging_unit": jQuery('input[name="packaging_unit[' + jQuery(row).val() + ']').val()
            }
          );
        }

      });
    }

  }

  if (orderIds && orderIds.length > 0) {
    createLoader();
    jQuery.ajax({
      type: "POST",
      url: deliveoApiLink,
      dataType: 'json',
      data: {
        selectedOrders: orderIds
      },
      success: function (response) {
        jQuery.each(response.success, function (indexInArray, message) {

          showSuccessMessage(message);
        });
        jQuery.each(response.errors, function (indexInArray, message) {
          showErrorMessage(message);
        });
        if (response.success.length > 0) {
          showSuccessMessage('Page will refresh in 5 seconds...');
          setTimeout(() => {
            window.location.reload();

          }, 5000);

        }
        removeLoader();
      }
    });
    // jQuery.post(deliveoApiLink, {
    //   selectedOrders: orderIds
    // })
    //   .done(function (result) {
    //     // window.location.reload();
    //   })
    //   .error(function () {
    //     removeLoader();
    //     alert('Hiba történt!');
    //   });
  }

}


function createLoader() {
  jQuery('body').append('<div class="deliveo-loader"><div class="loader-inner"><div class="lds-facebook"><div></div><div></div><div></div></div></div></div>');
}
jQuery(document).off('click', '.column-Deliveo').on('click', '.column-Deliveo', function (e) {
  e.preventDefault();
  var group_id = (jQuery(this).find('.badge').html());

  createWindow(group_id.replace(/\s/g, ''));
})
function createWindow(group_id) {
  jQuery('#deliveo_window').remove();
  jQuery('body').append('<div class="modal fade" id="deliveo_window"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h3 class="modal-title">Csomagcsoport részletei</h3></div><div class="modal-body"></div></div></div></div>');

  var signatureLink = deliveoUrl + 'signature/' + group_id + deliveoSlug;
  var labelLink = deliveoUrl + 'label/' + group_id + deliveoSlug;
  jQuery('#deliveo_window .modal-content').append('<div class="modal-footer"></div>');
  var invalid = 0;
  var dt = jQuery.get(signatureLink, function (data) {
    jQuery('#deliveo_window .modal-footer').append('<div class="btn-group pull-right">');

    try {
      if (data.type == "error") {
        jQuery('#deliveo_window #signature').remove();
      } else {
        invalid = 1;
      }
    } catch (e) {
      invalid = 1;
    }
  });
  if (invalid == 0) {
    jQuery('#deliveo_window .modal-footer').append('<a id="signature" class="btn btn-default" href="' + signatureLink + '"target="_blank" style="line-height:24px;padding:5px;" class="btn btn-success"><img style="height: 24px;vertical-align: middle;" src="/modules/deliveo/views/img/deliveo-signature.png">Aláíráslap letöltése</a>');
  }

  jQuery('#deliveo_window .modal-footer').append('<a class="btn btn-default" href="' + labelLink + '" target="_blank" style="line-height:24px; padding:5px;" class="btn btn-success"><img style="vertical-align: middle;height: 24px;" src="/modules/deliveo/views/img/deliveo-package-barcode.png">Csomagcímke letöltése</a>')
  jQuery('#deliveo_window .modal-footer').append('</div>')


  jQuery.ajax({
    type: "GET",
    url: deliveoPackageLog,
    data: {
      group_id: group_id
    },
    dataType: "json",
    success: function (response) {
      var table = "<table style='width:100%'>";
      table += "<tr><th style='text-align:left;padding:5px; border-bottom:1px solid;'>Időpont</th><th style='text-align:left;padding:5px; border-bottom:1px solid;'>Esemény</th></tr>"
      response.data.forEach(element => {
        if (element.status_text == "") {
          var text = element.status;
        } else {
          var text = "[" + element.status_text + "]";
        }
        table += "<tr><td style='font-weight: bold'>" + timeConverter(element.timestamp) + "</td><td>" + text + "</td></tr>";

      });
      if (response.data.length == 0) {
        table += "<tr><td style='font-weight:bold; text-align:center; padding: 5px;background-color: #ddd;' colspan='2'>Nem található naplóbejegyzés</td></tr>"
      }
      table += "</table>";
      jQuery('#deliveo_window .modal-body').append(table);
      jQuery('#deliveo_window .modal-title').text(group_id + " részletei");
    }
  });

  jQuery('#deliveo_window .modal-body').append();
  jQuery('#deliveo_window').modal('show');
}

function removeWindow() {
  jQuery('#deliveo_window').modal('hide');
  setTimeout(() => {
    jQuery('#deliveo_window').remove();
  }, 1000);
}

function removeLoader() {
  jQuery("body").find(".deliveo-loader").remove();
}


function timeConverter(timestamp) {
  var a = new Date(timestamp * 1000);
  var months = ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún', 'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var min = 10;
  var sec = a.getSeconds();
  if ((a.getMonth() + 1) < 10) {
    var month = "0" + (a.getMonth() + 1);
  }
  if ((a.getDate() + 1) < 10) {
    var date = "0" + (a.getDate());
  }
  if ((a.getHours() + 1) < 10) {
    var hour = "0" + (a.getHours());
  }
  if ((a.getMinutes()) < 10) {
    min = "0" + (a.getMinutes());
  }
  // var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
  var time = year + ". " + month + ". " + date + ". " + hour + ':' + min;
  return time;
}