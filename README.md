# Deliveo plugin for PrestaShop

# https://wiki.mav-it.hu/deliveo_prestashop_plugin

###### Telepítés:

1. Letöltés után kapunk egy zip fájlt: „deliveo-plugin-for-prestashop-master.zip”.
2. Csomagoljuk ki. Kicsomagolás után kapunk egy könyvtárat „deliveo-plugin-for-prestashop-master” néven.
3. Nevezzük át ezt a könyvtárat „deliveo”-ra, majd csomagoljuk vissza .zip fájlba. Kapunk egy „deliveo.zip” fájlt.
4. Navigáljunk az admin felületünk Modulok oldalára, a jobb felső sarokban kattintsunk a „Modul feltöltése gombra.
5. A deliveo.zip fájlt töltsük fel, a modul automatikusan települ.
6. A telepítés után frissítsük a „modulok” oldalt, majd keressük ki a Deliveo plugint, majd kattintsunk a „Konfigurálás” gombra. Adjuk meg a Licencünket és API kulcsunkat, mentsük el a beállításokat, majd töltsük ki a további beállítási mezőket is. Megjegyzés: A Haladó/Teljesítmény menüpont alatt a „Felülírások tiltása” beállítás „Nem” opciója legyen aktív.

Az átnevezéses műveletre azért van szükség, mert a Prestashop csak akkor engedi telepíteni a modult, ha a feltöltött .zip fájlban lévő könyvtár neve megegyezik a modul nevével, amit létre akar hozni. A GitLab viszont a repository nevét adja letöltéskor a tömörített fájlnak.

###### Install:
1. After downloading you will get a zip file: "deliveo-plugin-for-prestashop-master.zip".
2. Unpack. After unpacking, we get a directory called “deliveo-plugin-for-prestashop-master”.
3. Rename this directory to “deliveo” and then package it back into a .zip file. We get a "deliveo.zip" file.
4. Navigate to the Modules page of our admin interface, click on the “Upload Module” button in the upper right corner.
5. Upload the deliveo.zip file, the module will be installed automatically.
6. After installation, refresh the "Modules" page, find the Deliveo plugin and click the "Configure" button. Enter your License and API key, save the settings, and fill in the additional configuration fields. Note: Under Advanced / Performance, the "No Overwriting" option must be set to "No".

The rename operation is required because Prestashop only allows you to install the module if the directory name in the uploaded .zip file matches the name of the module you want to create. GitLab, on the other hand, gives the compressed file the name of the repository when it downloads.
